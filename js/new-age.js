(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 100
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 50
        }
    })
	// Hover Puff
	$( document ).hover(function() {
		$( "#toggle" ).toggle( "puff" );
	});
	
	// tooltip
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	//Popover
	$(function () {
	  $('[data-toggle="popover"]').popover()
	})

$(document).ready(function(){
		// Toggle
		$('#ingenieur').click(function(){
			$('#ecole').slideToggle(500);
		});
	
	
	// Auto Collapse for only one menu on time 	
	$('.grow, .letter, .parent-intro-category').click(function() {
        $('#initial, #realisation, #parcours, #quete, #contact' ).collapse('hide');
    });
		// Auto Collapse differant sections of #parcours
		$('.btn-parcours').click(function() {
			$('#parcours').children().collapse('hide');
		});
			// Auto Collapse for #ingenieur & #master
			$('#btn-master').click(function() {
				$('#ingenieur' ).collapse('hide');
			});
			$('#btn-ingenieur').click(function() {
				$('#master' ).collapse('hide');
			});
				// Auto Collapse for #labo & #indus
				
				$('#ingenieur > button').click(function() {
					$('#indus, #labo' ).collapse('hide');
				});
			// Auto Collapse for #entrepreneur
			$('#btn-projet, #btn-formation').click(function() {
					$('#projet, #formation' ).collapse('hide');
				});
				$('#btn-auberge, #btn-sciencease').click(function() {
					$('#auberge, #sciencease' ).collapse('hide');
				});
	// Opacity to exclude other DOM Version 1
	$(".btn-style").addClass("opacity");
	$(".btn-style").click(function(){		
		if ( ($(this).hasClass("opacity")) === true ) {
			$(this).parent().children("button").addClass( "opacity" );
			$(this).removeClass("opacity");				
		}
		else {
			$(this).addClass("opacity");	
		}
	});
	
});
	// Opacity to exclude other DOM Version 2

	// $(".btn-style").click(function(){
		// var btngroup = $(this).parent().children("button");
			
		// if (  (($(this).hasClass("selected")) === false) ) {
		// btngroup.addClass( "opacity" );
		// btngroup.removeClass( "selected" );
		// $(this).removeClass("opacity");		
		// $(this).addClass("selected");	
		// }
		// else {
			// $(this).addClass("opacity");		
			// $(this).removeClass("selected");
		// }
	// });
	
	// .text-muted color hover color change
		$(".text-muted").closest('button').mouseenter(function(){
			$(this).children(".text-muted").addClass("text-muted-color-change");
		});
		$(".text-muted").closest('button').mouseleave(function(){
			$(this).children(".text-muted").removeClass("text-muted-color-change");
		});
	
	// Realisations Panel
	
	// $(".panel-button").closest(".panel").click(function(){
		// $(this).addClass("panel-button-selected");
		// $('#panel-button-three').addClass("float-right");
		// $('#panel-button-one').addClass("float-left");
		
		// if ((this).hasClass("panel-button-selected")){
			// $(this).removeClass("panel-button-selected");
			// $('#panel-button-three').addClass("float-left");
			// $('#panel-button-one').addClass("float-right");
		// }
	// });

	
	
	// Shake arrow
	// $( '.intro-category' ).hover(function() {
		
		// $( ".intro-category" ).animate({ "left": "+=5px" }, "fast" );
		// $( ".intro-category" ).animate({ "left": "-=5px" }, "fast" );
	// });

})(jQuery); // End of use strict


